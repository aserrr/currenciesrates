//
//  UIColor+RGB.m
//  CurrenciesRates
//
//  Created by AseR on 29.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import "UIColor+RGB.h"

@implementation UIColor (RGB)

+(UIColor *) colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue A:(CGFloat)alpha {
    return [UIColor colorWithRed:(red/255.0f) green:(green/255.0f) blue:(blue/255.0f) alpha:alpha];
}

@end

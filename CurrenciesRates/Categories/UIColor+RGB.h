//
//  UIColor+RGB.h
//  CurrenciesRates
//
//  Created by AseR on 29.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RGB)

+(UIColor *) colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue A:(CGFloat)alpha;

@end

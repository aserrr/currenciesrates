//
//  UIButton+BottomLine.h
//  CurrenciesRates
//
//  Created by AseR on 29.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (BottomLine)

- (void)addBottomLineWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth;

@end

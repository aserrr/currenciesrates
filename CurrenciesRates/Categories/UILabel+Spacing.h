//
//  UILabel+Spacing.h
//  CurrenciesRates
//
//  Created by AseR on 30.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Spacing)

-(void)setText:(NSString *)text withSpacing:(double)spacing;
-(void)setText:(NSString *)text withLineSpacing:(double)spacing andAlignment:(NSTextAlignment)alignment;

@end

//
//  UIButton+BottomLine.m
//  CurrenciesRates
//
//  Created by AseR on 29.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import "UIButton+BottomLine.h"

@implementation UIButton (BottomLine)

- (void)addBottomLineWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth {
    CALayer* line = [CALayer layer];
    line.backgroundColor = color.CGColor;
    line.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width, borderWidth);
    [self.layer addSublayer:line];
}

@end

//
//  UILabel+Spacing.m
//  CurrenciesRates
//
//  Created by AseR on 30.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import "UILabel+Spacing.h"

@implementation UILabel (Spacing)

-(void)setText:(NSString *)text withSpacing:(double)spacing {
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedText addAttribute:NSKernAttributeName
                           value:[NSNumber numberWithDouble:spacing]
                           range:NSMakeRange(0, attributedText.length)];
    [self setAttributedText:attributedText];
}

-(void)setText:(NSString *)text withLineSpacing:(double)spacing andAlignment:(NSTextAlignment)alignment {
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    
    paragraphStyle.alignment = alignment;
    paragraphStyle.lineHeightMultiple = spacing;
    
    [attributedText addAttribute:NSParagraphStyleAttributeName
                           value:paragraphStyle
                           range:NSMakeRange(0, attributedText.length)];
    [self setAttributedText:attributedText];
}

@end

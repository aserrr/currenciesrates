//
//  ApiClient.h
//  CurrenciesRates
//
//  Created by AseR on 28.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ApiResponseBlock)(BOOL success, NSDictionary* result, NSError* error);

static NSString * const kRatesUrl = @"https://api.fixer.io/%@/?symbols=RUB,EUR,USD";
static NSString * const kBaseKey = @"base";
static NSString * const kRatesKey = @"rates";

static NSString * const kFromKey = @"from";
static NSString * const kToKey = @"to";
static NSString * const kRateKey = @"rate";
static NSString * const kDiffKey = @"diff";

@interface ApiClient : NSObject

+(void)fetchRatesForDate:(NSDate *)date completionHandler:(ApiResponseBlock)completionHandler;

@end

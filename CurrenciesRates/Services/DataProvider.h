//
//  DataProvider.h
//  CurrenciesRates
//
//  Created by AseR on 28.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"

typedef void(^ResponseBlock)(BOOL success, NSDictionary* result);

static NSString * const kStoredDataKey = @"storedData";
static NSString * const kStoredRatesKey = @"storedRates";
static NSString * const kStoredAtKey = @"storedAt";

@interface DataProvider : NSObject

+(void)currentRatesWithBlock:(ResponseBlock)completionHandler;

@end

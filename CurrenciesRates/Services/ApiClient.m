//
//  ApiClient.m
//  CurrenciesRates
//
//  Created by AseR on 28.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import "ApiClient.h"

@implementation ApiClient

+(void)fetchRatesForDate:(NSDate *)date completionHandler:(ApiResponseBlock)completionHandler {
    if (!completionHandler) {
        // Skip fetching cause of can't return a result
        return;
    }
    // Formate a date for request
    NSDateFormatter* dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *stringDate = [dateFormatter stringFromDate:date];
    
    // Setup a session tasl
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:kRatesUrl, stringDate]];
    NSURLSession* session = [NSURLSession sharedSession];
    NSURLSessionDataTask* sessionDataTask = [session dataTaskWithURL:url
                                                   completionHandler:^(NSData * _Nullable data,
                                                                       NSURLResponse * _Nullable response,
                                                                       NSError * _Nullable error) {
        if (error != nil) {
            // Task failed
            completionHandler(NO, nil, error);
        } else {
            // Try to parse JSON
            NSError *jsonError = nil;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (jsonError != nil) {
                // Parse failed
                completionHandler(NO, nil, jsonError);
            } else {
                // Build a result dictionary
                NSMutableDictionary* result = [NSMutableDictionary new];
                
                // Get data
                NSString* base = [json objectForKey:kBaseKey];
                NSDictionary* rates = [json objectForKey:kRatesKey];
                
                if ([base length] > 0 && [rates count] > 0) {
                    // Continue if data valid
                    NSArray* keys = [rates allKeys];
                    for (NSString* key in keys) {
                        NSNumber* rate = [rates objectForKey:key];
                        [result setObject:@{kFromKey: base, kToKey: key, kRateKey: rate}
                                   forKey:[base stringByAppendingString:key]];
                    }
                    
                    // Calculate cross rates
                    NSUInteger keysCount = [keys count];
                    if (keysCount > 1) {
                        for (int i = 0; i < keysCount - 1; i++) {
                            for (int j = i + 1; j < keysCount; j++) {
                                NSString* iKey = [keys objectAtIndex:i];
                                NSNumber* iRate = [rates objectForKey:iKey];
                                
                                NSString* jKey = [keys objectAtIndex:j];
                                NSNumber* jRate = [rates objectForKey:jKey];
                                
                                NSNumber* crossRate = [NSNumber numberWithDouble:iRate.doubleValue/jRate.doubleValue];
                                [result setObject:@{kFromKey: jKey, kToKey: iKey, kRateKey: crossRate}
                                           forKey:[jKey stringByAppendingString:iKey]];
                            }
                        }
                    }
                    
                    // Calculate reverse rates
                    NSMutableDictionary* reversed = [NSMutableDictionary new];
                    for (NSDictionary* item in [result allValues]) {
                        NSString* fromKey = [item objectForKey:kFromKey];
                        NSString* toKey = [item objectForKey:kToKey];
                        NSNumber* rate = [item objectForKey:kRateKey];
                        NSNumber* reversedRate = [NSNumber numberWithDouble:1/rate.doubleValue];
                        
                        [reversed setObject:@{kFromKey: toKey, kToKey: fromKey, kRateKey: reversedRate}
                                     forKey:[toKey stringByAppendingString:fromKey]];
                    }
                    
                    [result addEntriesFromDictionary:reversed];
                    
                    // return a result
                    completionHandler(YES, result, nil);
                }
            }
        }
        
    }];
    
    // Go
    [sessionDataTask resume];
}

@end

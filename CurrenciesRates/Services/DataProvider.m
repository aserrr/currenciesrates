//
//  DataProvider.m
//  CurrenciesRates
//
//  Created by AseR on 28.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import "DataProvider.h"

@implementation DataProvider

+(void)currentRatesWithBlock:(ResponseBlock)completionHandler {
    
    // ToDo:
    // We have to check a timezone and fetch correct data cause of rates are updated daily around 4PM CET
    NSDate* today = [NSDate date];
    NSDate* yesterday = [today dateByAddingTimeInterval: -1 * 60 * 60 * 24]; // 24 hours
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary* storedData = [userDefaults objectForKey:kStoredDataKey];
    BOOL sameDay = NO;
    
    if (storedData != nil) {
        // check a date
        NSDate* storedAt = [storedData objectForKey:kStoredAtKey];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        sameDay = [calendar isDate:storedAt inSameDayAsDate:today];
    }
    
    if (storedData != nil && sameDay) {
        // return cached data
        completionHandler(YES, storedData);
        
    } else {
        // fetch data
        [ApiClient fetchRatesForDate:today completionHandler:^(BOOL success, NSDictionary *resultForToday, NSError *error) {
            if (error || !success) {
                // handle error
            } else {
                // fetch a data for yesterday
                [ApiClient fetchRatesForDate:yesterday completionHandler:^(BOOL success, NSDictionary *resultForYesterday, NSError *error) {
                    if (success) {
                        // Got both
                        NSMutableDictionary* calculatedRates = [NSMutableDictionary new];
                        
                        // Calculate a difference
                        for (NSString* key in [resultForToday allKeys]) {
                            NSDictionary* todayData = [resultForToday objectForKey:key];
                            NSNumber* todayRate = [todayData objectForKey:kRateKey];
                            
                            NSDictionary* yesterdayData = [resultForYesterday objectForKey:key];
                            NSNumber* yesterdayRate = [yesterdayData objectForKey:kRateKey];
                            
                            double diff = (todayRate.doubleValue / yesterdayRate.doubleValue - 1) * 100;
                            [calculatedRates setObject:@{kFromKey: [todayData objectForKey:kFromKey],
                                                        kToKey: [todayData objectForKey:kToKey],
                                                        kRateKey: [todayData objectForKey:kRateKey],
                                                        kDiffKey: [NSNumber numberWithDouble:diff]}
                                               forKey:key];
                        }
                        
                        // Store results and return
                        NSDictionary* calculatedData = @{kStoredRatesKey: calculatedRates, kStoredAtKey: today};
                        [userDefaults setObject:calculatedData forKey:kStoredDataKey];
                        [userDefaults synchronize];
                        
                        completionHandler(YES, calculatedData);
                    }
                }];
            }
        }];
    }
}

@end

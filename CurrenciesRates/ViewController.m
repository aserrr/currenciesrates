//
//  ViewController.m
//  CurrenciesRates
//
//  Created by AseR on 27.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import "ViewController.h"
#import "DataProvider.h"
#import "UIColor+RGB.h"
#import "UIButton+BottomLine.h"
#import "UILabel+Spacing.h"

static NSTimeInterval const kAnimationDuration = 0.35;
static CGFloat const kTopOffsetDefault = 80.0f;
static CGFloat const kTopOffsetWithMenu = 15.0f;

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UILabel *currienciesLabel;
@property (strong, nonatomic) IBOutlet UILabel *rateLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentLabel;
@property (strong, nonatomic) IBOutlet UILabel *updatedLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *commentLabelWidthConstraint;

@property (strong, nonatomic) NSDictionary* currentRates;
@property (strong, nonatomic) NSString* selectedPair;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *currencyButtons;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *currencyButtonsWidthConstraints;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *currencyButtonsLeadingConstraints;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *currencyButtonsBottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *currenciesLabelTopConstraint;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.selectedPair = @"USDRUB";
    
    // setup labels
    self.currienciesLabel.font = [UIFont fontWithName:@"Lato-Bold" size:17];
    self.rateLabel.font = [UIFont fontWithName:@"Lato-Regular" size:80];
    self.commentLabel.font = [UIFont fontWithName:@"Lato-MediumItalic" size:17];
    self.updatedLabel.font = [UIFont fontWithName:@"Lato-Black" size:11];
    
    self.currienciesLabel.textColor = [UIColor colorWithR:63 G:71 B:83 A:0.7];
    self.rateLabel.textColor = [UIColor colorWithR:63 G:71 B:83 A:1];
    self.commentLabel.textColor = [UIColor colorWithR:126 G:211 B:33 A:1];
    self.updatedLabel.textColor = [UIColor colorWithR:63 G:71 B:83 A:0.4];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
    // fetch actual data
    [DataProvider currentRatesWithBlock:^(BOOL success, NSDictionary *result) {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update UI
                NSDate* storedAt = [result objectForKey:kStoredAtKey];
                NSDateFormatter* dateFormatter = [NSDateFormatter new];
                [dateFormatter setDateFormat:@"HH:mm"];
                NSString *stringTime = [dateFormatter stringFromDate:storedAt];
                
                NSString* updatedPlainText = [NSString stringWithFormat:NSLocalizedString(@"Updated at %@", nil), stringTime];
                [self.updatedLabel setText:updatedPlainText withSpacing:2.0];
                
                self.currentRates = [result objectForKey:kStoredRatesKey];
                [self updateLabelsForPair:self.selectedPair];
            });
        }
    }];

    // A little trick to set an equal width for Rate and Comment labels
    self.commentLabelWidthConstraint.constant = CGRectGetWidth(self.rateLabel.frame);
    
    // Adjust a width of buttons
    CGFloat width = CGRectGetWidth(self.view.frame);
    for (NSLayoutConstraint* widthConstraint in self.currencyButtonsWidthConstraints) {
        widthConstraint.constant = width;
    }
    
    // Add a border line for buttons
    for (UIButton* button in self.currencyButtons) {
        [button addBottomLineWithColor:[UIColor blackColor] andWidth:1.0];
    }
    
    // Hide buttons
    [self hideButtons];
}

- (IBAction)showMenu:(UIButton *)sender {
    [self configureButtons];
    // Animation
    [UIView animateWithDuration:kAnimationDuration animations:^{
        for (UIButton* button in self.currencyButtons) {
            button.alpha = 1.00f;
        }
        for (NSLayoutConstraint* leadingConstraint in self.currencyButtonsLeadingConstraints) {
            leadingConstraint.constant = 0;
        }
        self.currencyButtonsBottomConstraint.constant = 0;
        self.currenciesLabelTopConstraint.constant = kTopOffsetWithMenu;
        [self.view layoutIfNeeded];
    }];
}

- (void)configureButtons {
    for (UIButton* button in self.currencyButtons) {
        // Setup fonts
        if ([self.selectedPair isEqualToString:[self pairByTag:button.tag]]) {
            // Selected item
            button.titleLabel.font = [UIFont fontWithName:@"Lato-Black" size:28];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        } else {
            button.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:28];
            [button setTitleColor:[UIColor colorWithWhite:1 alpha:0.7] forState:UIControlStateNormal];
        }
    }
}

- (void)hideButtons {
    CGFloat totalHeihgt = 0.00f;
    for (UIButton* button in self.currencyButtons) {
        // Make buttons invisible
        button.alpha = 0.00f;
        totalHeihgt += CGRectGetHeight(button.frame);
    }
    
    // Move buttons out of screen
    CGFloat width = CGRectGetWidth(self.view.frame);
    int index = 0;
    for (NSLayoutConstraint* leadingConstraint in self.currencyButtonsLeadingConstraints) {
        leadingConstraint.constant = (index % 2 == 0) ? width : -width;
        index++;
    }
    self.currencyButtonsBottomConstraint.constant = -1 * totalHeihgt; // Move down
    
    // Set original offset
    self.currenciesLabelTopConstraint.constant = kTopOffsetDefault;
    [self.view layoutIfNeeded];
}

- (IBAction)currenciesPairSelected:(UIButton *)sender {
    // Save selection
    self.selectedPair = [self pairByTag:sender.tag];
    
    // Hide with animation
    [UIView animateWithDuration:kAnimationDuration animations:^{
        [self hideButtons];
    } completion:^(BOOL finished) {
        // Update labels when done
        [self updateLabelsForPair:self.selectedPair];
    }];
}

- (void)updateLabelsForPair:(NSString *)pair {
    NSDictionary* ratesValue = [self.currentRates objectForKey:pair];
    if (ratesValue == nil) {
        // Something went wrong
        return;
    }
    // Get data
    NSString* from = [ratesValue objectForKey:kFromKey];
    NSString* to = [ratesValue objectForKey:kToKey];
    NSNumber* rate = [ratesValue objectForKey:kRateKey];
    NSNumber* diff = [ratesValue objectForKey:kDiffKey];
    
    NSString* currienciesPlainText = [NSString stringWithFormat:@"%@ → %@", from, to];
    [self.currienciesLabel setText:currienciesPlainText withSpacing:1.0];
//    self.currienciesLabel.text = currienciesPlainText;
    
    // Use a comma as a decimal separator
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setDecimalSeparator:@","];
    
    NSString* ratePlainText = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:rate]];
    [self.rateLabel setText:ratePlainText withSpacing:-4.0];
//    self.rateLabel.text = ratePlainText;
    
    NSString* commentText = [self localizedCommentForValue:diff];
    if (diff.doubleValue < 0) {
        // Other color for negative values
        self.commentLabel.textColor = [UIColor colorWithR:208 G:2 B:27 A:1];
    } else {
        self.commentLabel.textColor = [UIColor colorWithR:126 G:211 B:33 A:1];
    }
    
    // Get a name of currency
    NSString* currency = NSLocalizedString(@"euro", nil);
    if ([from isEqualToString:@"USD"]) {
        currency = NSLocalizedString(@"dollar", nil);
    } else if ([from isEqualToString:@"RUB"]) {
        currency = NSLocalizedString(@"ruble", nil);
    }
    
    double spacing = 121.0 / 100.0;
    NSString* commentPlainText = [NSString stringWithFormat:commentText, currency, abs(diff.intValue)];
    [self.commentLabel setText:commentPlainText withLineSpacing:spacing andAlignment:NSTextAlignmentCenter];
//    self.commentLabel.text = commentPlainText;
}

#pragma mark - helpers

-(NSString *)pairByTag:(NSInteger)tag {
    // Tags are hardcoded in IB
    NSString* pair = @"USDRUB";
    switch (tag) {
        case 0:
            pair = @"USDRUB";
            break;
        case 1:
            pair = @"USDEUR";
            break;
        case 2:
            pair = @"EURRUB";
            break;
        case 3:
            pair = @"EURUSD";
            break;
        case 4:
            pair = @"RUBUSD";
            break;
        case 5:
            pair = @"RUBEUR";
            break;
            
        default:
            break;
    }
    return pair;
}

-(NSString *)localizedCommentForValue:(NSNumber *)value {
    if (NO && value.intValue == 0) { // Disabled cause of not required in specification
        return NSLocalizedString(@"There was no changes in percents scale", nil);
    } else {
        // Handle different cases for russian numerals
        int n = abs(value.intValue) % 100;
        int n1 = abs(value.intValue) % 10;
        
        if (n > 10 && n < 20) {
            return (value.doubleValue < 0) ? NSLocalizedString(@"fell by 5 percents", nil) : NSLocalizedString(@"rose by 5 percents", nil);
        }
        
        if (n1 > 1 && n1 < 5) {
            return (value.doubleValue < 0) ? NSLocalizedString(@"fell by 2 percents", nil) : NSLocalizedString(@"rose by 2 percents", nil);
        }
        
        if (n1 == 1) {
            return (value.doubleValue < 0) ? NSLocalizedString(@"fell by 1 percent", nil) : NSLocalizedString(@"rose by 1 percent", nil);
        }
    }
    return (value.doubleValue < 0) ? NSLocalizedString(@"fell by 5 percents", nil) : NSLocalizedString(@"rose by 5 percents", nil);
}

@end

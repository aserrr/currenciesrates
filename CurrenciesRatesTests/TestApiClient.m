//
//  TestApiClient.m
//  CurrenciesRates
//
//  Created by AseR on 30.06.16.
//  Copyright © 2016 AseR. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ApiClient.h"

@interface TestApiClient : XCTestCase

@end

@implementation TestApiClient

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// Some simple test
- (void)testApiResponse {
    __block BOOL waitingForData = YES;
    
    [ApiClient fetchRatesForDate:[NSDate date] completionHandler:^(BOOL success, NSDictionary *result, NSError *error) {
        waitingForData = NO;
        if (error != nil) {
            XCTFail(@"%@", error.description);
        }
        XCTAssert(success, @"Not successful response");
        
        if (success && !error) {
            XCTAssert([result allValues].count > 0, @"No data received");
        }
    }];
    
    while(waitingForData) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end

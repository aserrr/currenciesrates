ToDo:

- handle errors (API response, JSON parsing etc.)
- create a category for custom UIColor
- optimize a cache storage behavior (check if we already have a data for yesterday and do not fetch it)
- do not forget about network activity indicator
- check if today's data isn't available yet (if request a data before 4 PM CET)